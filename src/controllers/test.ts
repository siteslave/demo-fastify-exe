/// <reference path="../../typings.d.ts" />

import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import createError = require('http-errors')

export default async function test(fastify: FastifyInstance) {

  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'Test route!' })
  })

  fastify.get('/view', (request: FastifyRequest, reply: FastifyReply) => {
    reply.view('/views/index', { name: 'Satit Rianpit' })
  })

  fastify.get('/token', async (req: FastifyRequest, reply: FastifyReply) => {
    const token = fastify.jwt.sign({ foo: 'bar' }, { expiresIn: '1d' })
    reply.send({ token: token })
  })

  fastify.get('/private', {
    preHandler: [fastify.authenticate]
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    // Token decoded
    request.log.info(request.user)
    try {
      reply.code(200).send({ ok: true, message: 'Private data' })
    } catch (error) {
      request.log.error(error)
      reply.send(createError(500, error.message))
    }
  });

}

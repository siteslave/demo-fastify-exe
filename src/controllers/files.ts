/// <reference path="../../typings.d.ts" />

import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { v4 as uuidv4 } from 'uuid'

import * as fse from 'fs-extra'
import multer from 'fastify-multer'
import * as path from 'path'
import * as fs from 'fs'

export default async function files(fastify: FastifyInstance) {

  const uploadDir = process.env.UPLOAD_DIR || './uploads'

  fse.ensureDirSync(uploadDir)

  const storage = multer.diskStorage({
    destination: function (request: any, file: any, cb: any) {
      cb(null, uploadDir)
    },
    filename: function (request, file, cb) {
      const _ext = path.extname(file.originalname)
      cb(null, uuidv4() + _ext)
    }
  });

  const upload = multer({ storage: storage })


  fastify.post('/', {
    preHandler: [fastify.authenticate, upload.single('file')]
  }, (request: FastifyRequest, reply: FastifyReply) => {
    console.log(request.file)
    reply.send(request.file)
  })

  fastify.get('/:fileName', (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params
    const fileName: any = params.fileName

    const filePath = path.join(uploadDir, fileName)
    const mimeType = 'image/jpeg'

    try {
      if (fs.existsSync(filePath)) {
        reply.type(mimeType)
        const fileData = fs.readFileSync(filePath)
        reply.send(fileData)
      } else {
        reply.code(400).send({
          statusCode: 404,
          error: 'File not found'
        })
      }
    } catch (error) {
      reply.code(500).send({ statusCode: 500, error: error.message })
    }
  })

}

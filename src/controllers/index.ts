import { join } from 'path'

import * as os from 'os'
import * as fs from 'fs'
import * as fse from 'fs-extra'

const CryptoJS = require("crypto-js")

import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'

export default async function index(fastify: FastifyInstance) {

  const ENC_KEY = '@demo@'

  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'Hello wolrd!' })
  })

  fastify.get('/config', {
    preHandler: [fastify.authenticate]
  }, (request: FastifyRequest, reply: FastifyReply) => {
    const homeDirectory = os.homedir();

    const configFile = join(homeDirectory, 'my-config.json');
    const encryptText = fse.readFileSync(configFile);

    const bytes = CryptoJS.AES.decrypt(encryptText.toString(), ENC_KEY);

    const originalText = bytes.toString(CryptoJS.enc.Utf8);
    console.log('xxxx')
    console.log(originalText)
    const configData = JSON.parse(originalText);

    reply.send(configData)
  })

  fastify.post('/config', {
    preHandler: [fastify.authenticate]
  }, (request: FastifyRequest, reply: FastifyReply) => {

    const body: any = request.body
    const dbHost = body.dbHost
    const dbPort = body.dbPort
    const dbName = body.dbName
    const dbUser = body.dbUser
    const dbPassword = body.dbPassword
    const port = body.port
    const secretKey = body.secretKey

    const homeDirectory = os.homedir();

    const configFile = join(homeDirectory, 'my-config.json')

    const config = {
      database: {
        host: dbHost,
        name: dbName,
        port: +dbPort,
        user: dbUser,
        password: dbPassword
      },
      secretKey: secretKey,
      port: port
    }


    const ciphertext = CryptoJS.AES.encrypt(JSON.stringify(config), ENC_KEY).toString();

    fse.writeFileSync(configFile, ciphertext)

    reply.send({ ok: true })
  })

}

import { FastifyInstance } from 'fastify';

import indexRoute from './controllers/index'
import usersRoute from './controllers/users'
import filesRoute from './controllers/files'
import testRoute from './controllers/test'

export default async function router(fastify: FastifyInstance) {

  fastify.register(indexRoute, { prefix: '/' });
  fastify.register(usersRoute, { prefix: '/users' });
  fastify.register(filesRoute, { prefix: '/files' });
  fastify.register(testRoute, { prefix: '/test' });

}
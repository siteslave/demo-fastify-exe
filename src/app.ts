import * as fastify from 'fastify'
const CryptoJS = require("crypto-js")

import multer from 'fastify-multer'
import router from "./router"

import { join } from 'path'

import * as os from 'os'
import * as fs from 'fs'
import * as fse from 'fs-extra'

require('dotenv').config({ path: join(__dirname, '../config.conf') })

const app: fastify.FastifyInstance = fastify.fastify({
  logger: { level: 'info' }
})

app.register(require('fastify-formbody'))
app.register(require('fastify-cors'), {})
app.register(require('fastify-no-icon'))
app.register(multer.contentParser)

app.register(require('fastify-static'), {
  root: join(__dirname, '../public'),
  prefix: '/public/'
})

app.register(require('point-of-view'), {
  engine: {
    ejs: require('ejs'),
    root: join(__dirname, 'views')
  },
  includeViewExtension: true
})

app.register(require('fastify-rate-limit'), {
  max: +process.env.MAX_CONNECTION_PER_MINUTE || 500,
  timeWindow: '1 minute',
  whitelist: ['127.0.0.1', '192.168.1.1']
  // global: false
})


const homeDirectory = os.homedir();
const configFile = join(homeDirectory, 'my-config.json');

const ENC_KEY = '@demo@'

if (!fs.existsSync(configFile)) {
  const config = {
    database: {
      host: 'localhost',
      name: 'test',
      port: 3306,
      user: 'root',
      password: ''
    },
    secretKey: Math.round(Math.random() * 1000000),
    port: 3000
  }

  var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(config), ENC_KEY).toString();
  console.log(ciphertext)
  fse.writeFileSync(configFile, ciphertext)
}

const encryptText = fse.readFileSync(configFile);
const bytes = CryptoJS.AES.decrypt(encryptText.toString(), ENC_KEY);
const originalText = bytes.toString(CryptoJS.enc.Utf8);

const config = JSON.parse(originalText);

app.register(require('./plugins/jwt'), {
  secret: config.secretKey.toString()
})

// database connection
app.register(require('./plugins/db'), {
  connection: {
    client: 'mysql2',
    connection: {
      host: config.database.host || 'localhost',
      user: config.database.user || 'root',
      port: +config.database.port || 3306,
      password: config.database.password,
      database: config.database.name || 'test',
    },
    // pool: {
    //   min: 0,
    //   max: 100
    // },
    debug: +process.env.DEBUG === 1 ? true : false,
  },
  connectionName: 'db'
})

// import routers
app.register(router)

export default app;

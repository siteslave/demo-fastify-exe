import app from './app'
import { join } from 'path'

const CryptoJS = require("crypto-js")

import * as os from 'os'
import * as fs from 'fs'
import * as fse from 'fs-extra'

import jwt = require('jsonwebtoken')

const ENC_KEY = '@demo@'

const homeDirectory = os.homedir();

const configFile = join(homeDirectory, 'my-config.json');

if (!fs.existsSync(configFile)) {
    const config = {
        database: {
            host: 'localhost',
            name: 'test',
            port: 3306,
            user: 'root',
            password: ''
        },
        secretKey: Math.round(Math.random() * 1000000),
        port: 3000
    }

    var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(config), ENC_KEY).toString();

    fse.writeFileSync(configFile, ciphertext)
}

const encryptText = fse.readFileSync(configFile);

const bytes = CryptoJS.AES.decrypt(encryptText.toString(), ENC_KEY);

const originalText = bytes.toString(CryptoJS.enc.Utf8);

const configData = JSON.parse(originalText);

// listening configuration
const port = configData.port ? +configData.port : 3000
const host = '127.0.0.1'

const start = async () => {
    try {
        const secretKey = configData.secretKey
            ? configData.secretKey.toString()
            : Math.round(Math.random() * 1000000)
        const token = jwt.sign({ foo: 'bar' }, secretKey, { expiresIn: '2h' })
        console.log('==============')
        console.log('Token: ' + token)
        console.log('Config file: ' + configFile)
        console.log('==============')
        await app.listen(port, host)
        const info: any = app.server.address()
        //app.log.info(`Server listening on http://${info.address}:${info.port}`)
    } catch (err) {
        app.log.error(err)
        process.exit(1)
    }
}

start()
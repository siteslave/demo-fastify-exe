export interface IUserColumns {
  user_id?: string;
  cid?: string;
  first_name?: string;
  last_name?: string;
  sex?: string;
  birthdate?: string;
  phone_number?: string;
  email?: string;
  enable?: boolean;
  profile_image?: string;
  password?: string;
}

export interface IUserParams {
  userId?: string;
  cid?: string;
  firstName?: string;
  lastName?: string;
  sex?: string;
  birthdate?: string;
  phoneNumber?: string;
  email?: string;
  enable?: boolean;
  profileImage?: string;
  password?: string
}
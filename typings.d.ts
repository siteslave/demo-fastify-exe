import * as knex from 'knex'
import { Readable } from 'stream';

declare module 'fastify' {

  namespace Multer {
    interface File {
      fieldname: string;
      originalname: string;
      encoding: string;
      mimetype: string;
      size: number;
      stream: Readable;
      destination: string;
      filename: string;
      path: string;
      buffer: Buffer;
    }
  }

  interface FastifyRequest {
    user: any
    file: Multer.File
    jwtVerify: any
  }

  interface FastifyReply {
    view: any
  }

  interface FastifyInstance {
    db: knex
    authenticate: any
    jwt: any
  }
}

